## Adaptation of TurKontrol Into a Particle Filter

####Original Paper: 
###"Decision-Theoretic Control of Crowd-Sourced Workflows"
##### by Peng Dai, Mausam, and Daniel S. Weld (2010).
```
Author:          Ethan Petuchowski

Date Started:    6/18/13

License:         Unknown
```